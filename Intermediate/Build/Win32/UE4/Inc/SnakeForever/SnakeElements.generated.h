// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef SNAKEFOREVER_SnakeElements_generated_h
#error "SnakeElements.generated.h already included, missing '#pragma once' in SnakeElements.h"
#endif
#define SNAKEFOREVER_SnakeElements_generated_h

#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_SPARSE_DATA
#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_RPC_WRAPPERS \
	virtual void SetFirstElementType_Implementation(); \
 \
	DECLARE_FUNCTION(execToggleCallision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleCallision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_EVENT_PARMS
#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_CALLBACK_WRAPPERS
#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeElements(); \
	friend struct Z_Construct_UClass_ASnakeElements_Statics; \
public: \
	DECLARE_CLASS(ASnakeElements, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElements) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeElements*>(this); }


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASnakeElements(); \
	friend struct Z_Construct_UClass_ASnakeElements_Statics; \
public: \
	DECLARE_CLASS(ASnakeElements, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElements) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeElements*>(this); }


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeElements(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeElements) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElements); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElements); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElements(ASnakeElements&&); \
	NO_API ASnakeElements(const ASnakeElements&); \
public:


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElements(ASnakeElements&&); \
	NO_API ASnakeElements(const ASnakeElements&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElements); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElements); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeElements)


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_PRIVATE_PROPERTY_OFFSET
#define SnakeForever_Source_SnakeForever_SnakeElements_h_13_PROLOG \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_EVENT_PARMS


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_RPC_WRAPPERS \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_CALLBACK_WRAPPERS \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_INCLASS \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeForever_Source_SnakeForever_SnakeElements_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_CALLBACK_WRAPPERS \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_INCLASS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_SnakeElements_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEFOREVER_API UClass* StaticClass<class ASnakeElements>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeForever_Source_SnakeForever_SnakeElements_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
