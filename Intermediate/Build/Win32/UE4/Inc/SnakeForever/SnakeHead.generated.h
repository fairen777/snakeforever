// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEFOREVER_SnakeHead_generated_h
#error "SnakeHead.generated.h already included, missing '#pragma once' in SnakeHead.h"
#endif
#define SNAKEFOREVER_SnakeHead_generated_h

#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_SPARSE_DATA
#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMovementSpeedDown); \
	DECLARE_FUNCTION(execMovementSpeedUp); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execDelSnakeElement); \
	DECLARE_FUNCTION(execAddSnakeElement); \
	DECLARE_FUNCTION(execFirstSpawnSnake);


#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMovementSpeedDown); \
	DECLARE_FUNCTION(execMovementSpeedUp); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execDelSnakeElement); \
	DECLARE_FUNCTION(execAddSnakeElement); \
	DECLARE_FUNCTION(execFirstSpawnSnake);


#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeHead(); \
	friend struct Z_Construct_UClass_ASnakeHead_Statics; \
public: \
	DECLARE_CLASS(ASnakeHead, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ASnakeHead)


#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_INCLASS \
private: \
	static void StaticRegisterNativesASnakeHead(); \
	friend struct Z_Construct_UClass_ASnakeHead_Statics; \
public: \
	DECLARE_CLASS(ASnakeHead, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ASnakeHead)


#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeHead(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeHead) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeHead); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeHead); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeHead(ASnakeHead&&); \
	NO_API ASnakeHead(const ASnakeHead&); \
public:


#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeHead(ASnakeHead&&); \
	NO_API ASnakeHead(const ASnakeHead&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeHead); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeHead); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeHead)


#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_PRIVATE_PROPERTY_OFFSET
#define SnakeForever_Source_SnakeForever_SnakeHead_h_21_PROLOG
#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_RPC_WRAPPERS \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_INCLASS \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeForever_Source_SnakeForever_SnakeHead_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_INCLASS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_SnakeHead_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEFOREVER_API UClass* StaticClass<class ASnakeHead>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeForever_Source_SnakeForever_SnakeHead_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> SNAKEFOREVER_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
