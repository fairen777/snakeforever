// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeForever/Walls.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWalls() {}
// Cross Module References
	SNAKEFOREVER_API UClass* Z_Construct_UClass_AWalls_NoRegister();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_AWalls();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeForever();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AWalls::StaticRegisterNativesAWalls()
	{
	}
	UClass* Z_Construct_UClass_AWalls_NoRegister()
	{
		return AWalls::StaticClass();
	}
	struct Z_Construct_UClass_AWalls_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SafeZoneSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SafeZoneSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWalls_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeForever,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWalls_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Walls.h" },
		{ "ModuleRelativePath", "Walls.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWalls_Statics::NewProp_SafeZoneSize_MetaData[] = {
		{ "Category", "Walls" },
		{ "ModuleRelativePath", "Walls.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWalls_Statics::NewProp_SafeZoneSize = { "SafeZoneSize", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWalls, SafeZoneSize), METADATA_PARAMS(Z_Construct_UClass_AWalls_Statics::NewProp_SafeZoneSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWalls_Statics::NewProp_SafeZoneSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWalls_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWalls_Statics::NewProp_SafeZoneSize,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWalls_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AWalls, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWalls_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWalls>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWalls_Statics::ClassParams = {
		&AWalls::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWalls_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWalls_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWalls_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWalls_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWalls()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWalls_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWalls, 1756852907);
	template<> SNAKEFOREVER_API UClass* StaticClass<AWalls>()
	{
		return AWalls::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWalls(Z_Construct_UClass_AWalls, &AWalls::StaticClass, TEXT("/Script/SnakeForever"), TEXT("AWalls"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWalls);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
