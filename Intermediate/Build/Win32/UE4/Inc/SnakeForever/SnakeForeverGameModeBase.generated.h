// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEFOREVER_SnakeForeverGameModeBase_generated_h
#error "SnakeForeverGameModeBase.generated.h already included, missing '#pragma once' in SnakeForeverGameModeBase.h"
#endif
#define SNAKEFOREVER_SnakeForeverGameModeBase_generated_h

#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_SPARSE_DATA
#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_RPC_WRAPPERS
#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeForeverGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeForeverGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeForeverGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ASnakeForeverGameModeBase)


#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeForeverGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeForeverGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeForeverGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ASnakeForeverGameModeBase)


#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeForeverGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeForeverGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeForeverGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeForeverGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeForeverGameModeBase(ASnakeForeverGameModeBase&&); \
	NO_API ASnakeForeverGameModeBase(const ASnakeForeverGameModeBase&); \
public:


#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeForeverGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeForeverGameModeBase(ASnakeForeverGameModeBase&&); \
	NO_API ASnakeForeverGameModeBase(const ASnakeForeverGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeForeverGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeForeverGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeForeverGameModeBase)


#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_12_PROLOG
#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_RPC_WRAPPERS \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_INCLASS \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEFOREVER_API UClass* StaticClass<class ASnakeForeverGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeForever_Source_SnakeForever_SnakeForeverGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
