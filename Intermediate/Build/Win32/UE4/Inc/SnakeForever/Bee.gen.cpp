// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeForever/Bee.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBee() {}
// Cross Module References
	SNAKEFOREVER_API UClass* Z_Construct_UClass_ABee_NoRegister();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_ABee();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_AFood();
	UPackage* Z_Construct_UPackage__Script_SnakeForever();
// End Cross Module References
	void ABee::StaticRegisterNativesABee()
	{
	}
	UClass* Z_Construct_UClass_ABee_NoRegister()
	{
		return ABee::StaticClass();
	}
	struct Z_Construct_UClass_ABee_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABee_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AFood,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeForever,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABee_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Bee.h" },
		{ "ModuleRelativePath", "Bee.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABee_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABee>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABee_Statics::ClassParams = {
		&ABee::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABee_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABee_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABee()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABee_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABee, 1598180623);
	template<> SNAKEFOREVER_API UClass* StaticClass<ABee>()
	{
		return ABee::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABee(Z_Construct_UClass_ABee, &ABee::StaticClass, TEXT("/Script/SnakeForever"), TEXT("ABee"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABee);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
