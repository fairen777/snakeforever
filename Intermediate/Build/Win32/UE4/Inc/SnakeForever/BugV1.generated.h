// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEFOREVER_BugV1_generated_h
#error "BugV1.generated.h already included, missing '#pragma once' in BugV1.h"
#endif
#define SNAKEFOREVER_BugV1_generated_h

#define SnakeForever_Source_SnakeForever_BugV1_h_15_SPARSE_DATA
#define SnakeForever_Source_SnakeForever_BugV1_h_15_RPC_WRAPPERS
#define SnakeForever_Source_SnakeForever_BugV1_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeForever_Source_SnakeForever_BugV1_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABugV1(); \
	friend struct Z_Construct_UClass_ABugV1_Statics; \
public: \
	DECLARE_CLASS(ABugV1, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ABugV1)


#define SnakeForever_Source_SnakeForever_BugV1_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABugV1(); \
	friend struct Z_Construct_UClass_ABugV1_Statics; \
public: \
	DECLARE_CLASS(ABugV1, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ABugV1)


#define SnakeForever_Source_SnakeForever_BugV1_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABugV1(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABugV1) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABugV1); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABugV1); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABugV1(ABugV1&&); \
	NO_API ABugV1(const ABugV1&); \
public:


#define SnakeForever_Source_SnakeForever_BugV1_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABugV1() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABugV1(ABugV1&&); \
	NO_API ABugV1(const ABugV1&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABugV1); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABugV1); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABugV1)


#define SnakeForever_Source_SnakeForever_BugV1_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeForever_Source_SnakeForever_BugV1_h_12_PROLOG
#define SnakeForever_Source_SnakeForever_BugV1_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_BugV1_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_BugV1_h_15_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_BugV1_h_15_RPC_WRAPPERS \
	SnakeForever_Source_SnakeForever_BugV1_h_15_INCLASS \
	SnakeForever_Source_SnakeForever_BugV1_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeForever_Source_SnakeForever_BugV1_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_BugV1_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_BugV1_h_15_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_BugV1_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_BugV1_h_15_INCLASS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_BugV1_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEFOREVER_API UClass* StaticClass<class ABugV1>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeForever_Source_SnakeForever_BugV1_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
