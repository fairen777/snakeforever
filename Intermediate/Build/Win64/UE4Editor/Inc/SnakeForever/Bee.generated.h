// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEFOREVER_Bee_generated_h
#error "Bee.generated.h already included, missing '#pragma once' in Bee.h"
#endif
#define SNAKEFOREVER_Bee_generated_h

#define SnakeForever_Source_SnakeForever_Bee_h_15_SPARSE_DATA
#define SnakeForever_Source_SnakeForever_Bee_h_15_RPC_WRAPPERS
#define SnakeForever_Source_SnakeForever_Bee_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeForever_Source_SnakeForever_Bee_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABee(); \
	friend struct Z_Construct_UClass_ABee_Statics; \
public: \
	DECLARE_CLASS(ABee, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ABee)


#define SnakeForever_Source_SnakeForever_Bee_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABee(); \
	friend struct Z_Construct_UClass_ABee_Statics; \
public: \
	DECLARE_CLASS(ABee, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(ABee)


#define SnakeForever_Source_SnakeForever_Bee_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABee(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABee) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABee); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABee); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABee(ABee&&); \
	NO_API ABee(const ABee&); \
public:


#define SnakeForever_Source_SnakeForever_Bee_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABee() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABee(ABee&&); \
	NO_API ABee(const ABee&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABee); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABee); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABee)


#define SnakeForever_Source_SnakeForever_Bee_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeForever_Source_SnakeForever_Bee_h_12_PROLOG
#define SnakeForever_Source_SnakeForever_Bee_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_Bee_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_Bee_h_15_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_Bee_h_15_RPC_WRAPPERS \
	SnakeForever_Source_SnakeForever_Bee_h_15_INCLASS \
	SnakeForever_Source_SnakeForever_Bee_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeForever_Source_SnakeForever_Bee_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_Bee_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_Bee_h_15_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_Bee_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_Bee_h_15_INCLASS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_Bee_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEFOREVER_API UClass* StaticClass<class ABee>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeForever_Source_SnakeForever_Bee_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
