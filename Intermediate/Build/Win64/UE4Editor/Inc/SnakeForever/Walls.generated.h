// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEFOREVER_Walls_generated_h
#error "Walls.generated.h already included, missing '#pragma once' in Walls.h"
#endif
#define SNAKEFOREVER_Walls_generated_h

#define SnakeForever_Source_SnakeForever_Walls_h_13_SPARSE_DATA
#define SnakeForever_Source_SnakeForever_Walls_h_13_RPC_WRAPPERS
#define SnakeForever_Source_SnakeForever_Walls_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeForever_Source_SnakeForever_Walls_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWalls(); \
	friend struct Z_Construct_UClass_AWalls_Statics; \
public: \
	DECLARE_CLASS(AWalls, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(AWalls) \
	virtual UObject* _getUObject() const override { return const_cast<AWalls*>(this); }


#define SnakeForever_Source_SnakeForever_Walls_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAWalls(); \
	friend struct Z_Construct_UClass_AWalls_Statics; \
public: \
	DECLARE_CLASS(AWalls, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeForever"), NO_API) \
	DECLARE_SERIALIZER(AWalls) \
	virtual UObject* _getUObject() const override { return const_cast<AWalls*>(this); }


#define SnakeForever_Source_SnakeForever_Walls_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWalls(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWalls) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWalls); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWalls); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWalls(AWalls&&); \
	NO_API AWalls(const AWalls&); \
public:


#define SnakeForever_Source_SnakeForever_Walls_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWalls(AWalls&&); \
	NO_API AWalls(const AWalls&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWalls); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWalls); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWalls)


#define SnakeForever_Source_SnakeForever_Walls_h_13_PRIVATE_PROPERTY_OFFSET
#define SnakeForever_Source_SnakeForever_Walls_h_10_PROLOG
#define SnakeForever_Source_SnakeForever_Walls_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_Walls_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_Walls_h_13_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_Walls_h_13_RPC_WRAPPERS \
	SnakeForever_Source_SnakeForever_Walls_h_13_INCLASS \
	SnakeForever_Source_SnakeForever_Walls_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeForever_Source_SnakeForever_Walls_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeForever_Source_SnakeForever_Walls_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeForever_Source_SnakeForever_Walls_h_13_SPARSE_DATA \
	SnakeForever_Source_SnakeForever_Walls_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_Walls_h_13_INCLASS_NO_PURE_DECLS \
	SnakeForever_Source_SnakeForever_Walls_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEFOREVER_API UClass* StaticClass<class AWalls>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeForever_Source_SnakeForever_Walls_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
