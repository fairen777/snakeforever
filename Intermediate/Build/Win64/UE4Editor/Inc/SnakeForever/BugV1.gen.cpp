// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeForever/BugV1.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBugV1() {}
// Cross Module References
	SNAKEFOREVER_API UClass* Z_Construct_UClass_ABugV1_NoRegister();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_ABugV1();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_AFood();
	UPackage* Z_Construct_UPackage__Script_SnakeForever();
// End Cross Module References
	void ABugV1::StaticRegisterNativesABugV1()
	{
	}
	UClass* Z_Construct_UClass_ABugV1_NoRegister()
	{
		return ABugV1::StaticClass();
	}
	struct Z_Construct_UClass_ABugV1_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABugV1_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AFood,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeForever,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABugV1_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BugV1.h" },
		{ "ModuleRelativePath", "BugV1.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABugV1_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABugV1>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABugV1_Statics::ClassParams = {
		&ABugV1::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABugV1_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABugV1_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABugV1()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABugV1_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABugV1, 2722872190);
	template<> SNAKEFOREVER_API UClass* StaticClass<ABugV1>()
	{
		return ABugV1::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABugV1(Z_Construct_UClass_ABugV1, &ABugV1::StaticClass, TEXT("/Script/SnakeForever"), TEXT("ABugV1"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABugV1);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
