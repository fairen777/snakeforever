// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeForever/BugV2.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBugV2() {}
// Cross Module References
	SNAKEFOREVER_API UClass* Z_Construct_UClass_ABugV2_NoRegister();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_ABugV2();
	SNAKEFOREVER_API UClass* Z_Construct_UClass_AFood();
	UPackage* Z_Construct_UPackage__Script_SnakeForever();
// End Cross Module References
	void ABugV2::StaticRegisterNativesABugV2()
	{
	}
	UClass* Z_Construct_UClass_ABugV2_NoRegister()
	{
		return ABugV2::StaticClass();
	}
	struct Z_Construct_UClass_ABugV2_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABugV2_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AFood,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeForever,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABugV2_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BugV2.h" },
		{ "ModuleRelativePath", "BugV2.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABugV2_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABugV2>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABugV2_Statics::ClassParams = {
		&ABugV2::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABugV2_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABugV2_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABugV2()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABugV2_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABugV2, 3797420759);
	template<> SNAKEFOREVER_API UClass* StaticClass<ABugV2>()
	{
		return ABugV2::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABugV2(Z_Construct_UClass_ABugV2, &ABugV2::StaticClass, TEXT("/Script/SnakeForever"), TEXT("ABugV2"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABugV2);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
