// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeForever.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeForever, "SnakeForever" );
