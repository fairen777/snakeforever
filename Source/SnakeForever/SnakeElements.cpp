// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElements.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeHead.h"

// Sets default values
ASnakeElements::ASnakeElements()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElements::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASnakeElements::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASnakeElements::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElements::HandleBeginOverlap);
}

void ASnakeElements::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeHead>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

void ASnakeElements::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
										AActor* OtherActor, 
										UPrimitiveComponent* OtherComp, 
										int32 OtherIndexBady, 
										bool bFromSweep, 
										const FHitResult& SweepResult)
{
	if (IsValid(SnakeOvner))
	{
		SnakeOvner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElements::ToggleCallision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}
