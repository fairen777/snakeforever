// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeHead.generated.h"

class ASnakeElements;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};


UCLASS()
class SNAKEFOREVER_API ASnakeHead : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeHead();
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf <ASnakeElements> SnakeElementsClass;
	
	UPROPERTY()
		TArray <ASnakeElements*> SnakeElemArray;
		
	int32 LastElement;
	
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	
	UPROPERTY()
		EMovementDirection LastMoveDirection;
	
	UPROPERTY(EditDefaultsOnly)
		float TickInt;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void FirstSpawnSnake(int ElementsNum);
	
	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);
	
	UFUNCTION(BlueprintCallable)
		void DelSnakeElement();

	UFUNCTION(BlueprintCallable)
		void Move();
	
	UFUNCTION(BlueprintCallable)
		void MovementSpeedUp();
	
	UFUNCTION(BlueprintCallable)
		void MovementSpeedDown();

		void SnakeElementOverlap(ASnakeElements* OverlappedElement, AActor* Other);
};
