// Fill out your copyright notice in the Description page of Project Settings.


#include "BugV2.h"
#include "SnakeHead.h"


void ABugV2::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeHead>(Interactor);

		if (IsValid(Snake))
		{
			Destroy();
			Snake->AddSnakeElement();
			Snake->MovementSpeedUp(); //chage speed snake's movement

		}
	}
}
