// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "BugV1.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEFOREVER_API ABugV1 : public AFood
{
	GENERATED_BODY()
	
		void Interact(AActor* Interactor, bool bIsHead);
};
