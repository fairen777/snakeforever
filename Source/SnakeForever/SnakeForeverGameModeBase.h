// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeForeverGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEFOREVER_API ASnakeForeverGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
