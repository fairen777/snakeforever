// Fill out your copyright notice in the Description page of Project Settings.


#include "Bee.h"
#include "SnakeHead.h"

void ABee::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeHead>(Interactor);

		if (IsValid(Snake))
		{
			Destroy();
			Snake->DelSnakeElement();
			Snake->MovementSpeedDown();
		}
	}
}