// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeHead.h"
#include <random>
#include <ctime>

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//srand(time(0));
	//EventIndex = rand() % 4;
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeHead>(Interactor);
		
		if (IsValid(Snake))
		{
			Destroy();
		}
	}
}

