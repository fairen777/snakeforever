// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Bee.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEFOREVER_API ABee : public AFood
{
	GENERATED_BODY()

	
		void Interact(AActor* Interactor, bool bIsHead);
	

};
