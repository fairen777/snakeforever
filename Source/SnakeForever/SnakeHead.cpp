// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeHead.h"
#include "SnakeElements.h"
#include "Interactable.h"

// Sets default values
ASnakeHead::ASnakeHead()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	TickInt = 0.5;
	LastMoveDirection = EMovementDirection::DOWN;
	LastElement=0;

}

// Called when the game starts or when spawned
void ASnakeHead::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(TickInt);
	FirstSpawnSnake(3);
}

// Called every frame
void ASnakeHead::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeHead::FirstSpawnSnake(int ElementNum)
{
	for (int i = 0; i < ElementNum; ++i)
	{
		FVector NewLocation(SnakeElemArray.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElements* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElements>(SnakeElementsClass, NewTransform);
		NewSnakeElem->SnakeOvner = this;
		LastElement = SnakeElemArray.Add(NewSnakeElem);
		
				
		if (LastElement == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}

	}
}

void ASnakeHead::AddSnakeElement(int ElementNum)
{
	for (int i = 0; i < ElementNum; ++i)
	{
		auto CurrentElement = SnakeElemArray.Last(0);
		FVector CurrentLocation = CurrentElement->MeshComponent->GetComponentLocation();
		FVector NewLocation(CurrentLocation.X, CurrentLocation.Y, CurrentLocation.Z);
		FTransform NewTransform(NewLocation);
		ASnakeElements* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElements>(SnakeElementsClass, NewTransform);
		NewSnakeElem->SnakeOvner = this;
		LastElement = SnakeElemArray.Add(NewSnakeElem);

	}
}

void ASnakeHead::DelSnakeElement()
{
	
	
	if (LastElement>1)
	{
		SnakeElemArray.Last(0)->Destroy();
		SnakeElemArray.SetNum(LastElement);
		LastElement -= 1;
	}
}

void ASnakeHead::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElemArray[0]->ToggleCallision();

	
	for (int i = SnakeElemArray.Num()-1; i>0; i--) 
	{
		auto CurrentElement = SnakeElemArray[i];
		auto PrevElement = SnakeElemArray[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElemArray[0]->AddActorWorldOffset(MovementVector);
	SnakeElemArray[0]->ToggleCallision();
}

void ASnakeHead::MovementSpeedUp()
{
	if (TickInt>0.3)
	{
		SetActorTickInterval(TickInt -= 0.1);
	}
	

}

void ASnakeHead::MovementSpeedDown()
{
	if (TickInt<1.2)
	{
		SetActorTickInterval(TickInt += 0.1);
	}
	
}

void ASnakeHead::SnakeElementOverlap(ASnakeElements* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElemArray.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

