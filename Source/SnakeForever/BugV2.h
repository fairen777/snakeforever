// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "BugV2.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEFOREVER_API ABugV2 : public AFood
{
	GENERATED_BODY()
	
		void Interact(AActor* Interactor, bool bIsHead);

};
